package javac.parser;

import java.io.InputStream;
import javac.absyn.*;
import javac.util.*;

action code {:
	static javac.symbol.Symbol sym(String s) {
		return javac.symbol.Symbol.valueOf(s);
	}
	
	static javac.util.Position pos(int line, int column) {
		return javac.util.Position.valueOf(line, column);
	}
:};

parser code {:
	public void reportError(String message, Object info) {
		final StringBuffer m = new StringBuffer("Parsing error");
		if (info instanceof java_cup.runtime.Symbol) {
			final java_cup.runtime.Symbol s = (java_cup.runtime.Symbol) info;
			if (s.left >= 0) {
				m.append(" in line " + (s.left + 1));
				if (s.right >= 0) {
					m.append(", column " + (s.right + 1));
				}
			}
		}
		m.append(" : " + message);
		System.err.println(m);
	}
	
	public void reportFatalError(String message, Object info) {
		reportError(message, info);
		System.exit(1);
	}
	
	public Parser(InputStream inStream) {
		this(new Yylex(inStream));
	}
:};

terminal            NATIVE, RECORD, NEW, INT, STRING, CHAR, NULL, IF, ELSE, WHILE, FOR, RETURN, BREAK, CONTINUE;
terminal            SEMICOLON, LBRACE, RBRACE, LPAREN, RPAREN, COMMA, LBRACKET, RBRACKET, LRBRACKET, ASSIGN, OR, AND;
terminal            EQ, NEQ, LESS, LESS_EQ, GREATER, GREATER_EQ, PLUS, MINUS, MULTIPLY, DIVIDE, MODULO, NOT, DOT;
terminal Integer    INTEGER;
terminal String     ID, STRING_LITERAL;
terminal Character  CHARACTER;

non terminal TranslationUnit   translation_unit;
non terminal ExternalDecl      external_decl;
non terminal PrototypeDecl     prototype_decl;
non terminal FunctionDef       function_def;
non terminal RecordDef         record_def;
non terminal VariableDeclList  variable_decl_list;
non terminal FunctionHead      function_head;
non terminal ParameterList     parameter_list;
non terminal ParameterDecl     parameter_decl;
non terminal VariableDecl      variable_decl;
non terminal TypeSpecifier     type_specifier;
non terminal IdList            id_list;
non terminal StmtList          stmt_list;
non terminal Stmt              stmt;
non terminal CompoundStmt      compound_stmt;
non terminal ExprStmt          expr_stmt;
non terminal IfStmt            if_stmt;
non terminal WhileStmt         while_stmt;
non terminal ForStmt           for_stmt;
non terminal Expr              expr;
non terminal Expr              assignment_expr;
non terminal Expr              logical_or_expr;
non terminal Expr              logical_and_expr;
non terminal Expr              equality_expr;
non terminal Expr              relational_expr;
non terminal Expr              additive_expr;
non terminal Expr              mult_expr;
non terminal Expr              unary_expr;
non terminal Expr              postfix;
non terminal Expr              primary;

precedence right  ELSE;
precedence left   LBRACKET;

start with translation_unit;

translation_unit ::= external_decl:e	{: RESULT = new TranslationUnit(pos(eleft, eright)); RESULT.add(e); :}
                   | translation_unit:t external_decl:e	{: RESULT = t; t.add(e); :}
                   ;

external_decl ::= prototype_decl:p	{: RESULT = p; :}
                | function_def:f	{: RESULT = f; :}
                | record_def:r	{: RESULT = r; :}
                ;

prototype_decl ::= NATIVE:n function_head:f SEMICOLON	{: RESULT = new PrototypeDecl(pos(nleft, nright), f); :}
                 ;

function_def ::= function_head:f LBRACE   variable_decl_list:v stmt_list:s RBRACE	{: RESULT = new FunctionDef(pos(fleft, fright), f, v, s); :}
               | function_head:f LBRACE:l                      stmt_list:s RBRACE	{: RESULT = new FunctionDef(pos(fleft, fright), f, new VariableDeclList(pos(lleft, lright)), s); :}
               ;

record_def ::= RECORD:r ID:i LBRACE variable_decl_list:v RBRACE	{: RESULT = new RecordDef(pos(rleft, rright), sym(i), v); :}
             ;

variable_decl_list ::= variable_decl:v	{: RESULT = new VariableDeclList(pos(vleft, vright)); RESULT.add(v); :}
                     | variable_decl_list:l variable_decl:v	{: RESULT = l; l.add(v); :}
                     ;

function_head ::= type_specifier:t ID:i LPAREN   parameter_list:p RPAREN	{: RESULT = new FunctionHead(pos(tleft, tright), t, sym(i), p); :}
                | type_specifier:t ID:i LPAREN:l                  RPAREN	{: RESULT = new FunctionHead(pos(tleft, tright), t, sym(i), new ParameterList(pos(lleft, lright))); :}
                ;

parameter_list ::= parameter_decl:p	{: RESULT = new ParameterList(pos(pleft, pright)); RESULT.add(p); :}
                 | parameter_list:l COMMA parameter_decl:p	{: RESULT = l; l.add(p); :}
                 ;

parameter_decl ::= type_specifier:t ID:i	{: RESULT = new ParameterDecl(pos(tleft, tright), t, sym(i)); :}
                 ;

variable_decl ::= type_specifier:t id_list:i SEMICOLON	{: RESULT = new VariableDecl(pos(tleft, tright), t, i); :}
                ;

type_specifier ::= INT:i	{: RESULT = new IntType(pos(ileft, iright)); :}
                 | STRING:s	{: RESULT = new StringType(pos(sleft, sright)); :}
                 | CHAR:c	{: RESULT = new CharType(pos(cleft, cright)); :}
                 | ID:i	{: RESULT = new IdType(pos(ileft, iright), sym(i)); :}
                 | type_specifier:t LRBRACKET	{: RESULT = new ArrayType(pos(tleft, tright), t); :}
                 ;

id_list ::= ID:i	{: RESULT = new IdList(pos(ileft, iright)); RESULT.add(sym(i)); :}
          | id_list:l COMMA ID:i	{: RESULT = l; l.add(sym(i)); :}
          ;

stmt_list ::= stmt:s	{: RESULT = new StmtList(pos(sleft, sright)); RESULT.add(s); :}
            | stmt_list:l stmt:s	{: RESULT = l; l.add(s); :}
            ;

stmt ::= compound_stmt:c	{: RESULT = c; :}
       | expr_stmt:e	{: RESULT = e; :}
       | if_stmt:i	{: RESULT = i; :}
       | while_stmt:w	{: RESULT = w; :}
       | for_stmt:f	{: RESULT = f; :}
       | RETURN:r expr:e SEMICOLON	{: RESULT = new ReturnStmt(pos(rleft, rright), e); :}
       | BREAK:b SEMICOLON	{: RESULT = new BreakStmt(pos(bleft, bright)); :}
       | CONTINUE:c SEMICOLON	{: RESULT = new ContinueStmt(pos(cleft, cright)); :}
       ;

compound_stmt ::= LBRACE:l stmt_list:s RBRACE	{: RESULT = new CompoundStmt(pos(lleft, lright), s); :}
                | LBRACE:l             RBRACE	{: RESULT = new CompoundStmt(pos(lleft, lright), new StmtList(pos(lleft, lright))); :}
                ;

expr_stmt ::= expr:e SEMICOLON	{: RESULT = new ExprStmt(pos(eleft, eright), e); :}
            ;

if_stmt ::= IF:i LPAREN expr:e RPAREN stmt:s	{: RESULT = new IfStmt(pos(ileft, iright), e, s, Stmt.nop); :}
          | IF:i LPAREN expr:e RPAREN stmt:s ELSE stmt:t	{: RESULT = new IfStmt(pos(ileft, iright), e, s, t); :}
          ;

while_stmt ::= WHILE:w LPAREN expr:e RPAREN stmt:s	{: RESULT = new WhileStmt(pos(wleft, wright), e, s); :}
             ;

for_stmt ::= FOR:f LPAREN expr_stmt:a expr_stmt:b expr:c RPAREN stmt:s	{: RESULT = new ForStmt(pos(fleft, fright), a.getExpr(), b.getExpr(), c, s); :}
           | FOR:f LPAREN expr_stmt:a expr_stmt:b        RPAREN stmt:s	{: RESULT = new ForStmt(pos(fleft, fright), a.getExpr(), b.getExpr(), Null.nop, s); :}
           | FOR:f LPAREN expr_stmt:a SEMICOLON:m expr:c RPAREN stmt:s	{: RESULT = new ForStmt(pos(fleft, fright), a.getExpr(), new IntLiteral(pos(mleft, mright), 1), c, s); :}
           | FOR:f LPAREN expr_stmt:a SEMICOLON:m        RPAREN stmt:s	{: RESULT = new ForStmt(pos(fleft, fright), a.getExpr(), new IntLiteral(pos(mleft, mright), 1), Null.nop, s); :}
           | FOR:f LPAREN SEMICOLON   expr_stmt:b expr:c RPAREN stmt:s	{: RESULT = new ForStmt(pos(fleft, fright), Null.nop, b.getExpr(), c, s); :}
           | FOR:f LPAREN SEMICOLON   expr_stmt:b        RPAREN stmt:s	{: RESULT = new ForStmt(pos(fleft, fright), Null.nop, b.getExpr(), Null.nop, s); :}
           | FOR:f LPAREN SEMICOLON   SEMICOLON:m expr:c RPAREN stmt:s	{: RESULT = new ForStmt(pos(fleft, fright), Null.nop, new IntLiteral(pos(mleft, mright), 1), c, s); :}
           | FOR:f LPAREN SEMICOLON   SEMICOLON:m        RPAREN stmt:s	{: RESULT = new ForStmt(pos(fleft, fright), Null.nop, new IntLiteral(pos(mleft, mright), 1), Null.nop, s); :}
           ;

expr ::= assignment_expr:e	{: RESULT = e; :}
       | expr:x COMMA:o assignment_expr:y	{: RESULT = new BinaryExpr(pos(oleft, oright), x, BinaryOp.COMMA, y); :}
       ;

assignment_expr ::= logical_or_expr:e	{: RESULT = e; :}
                  | unary_expr:x ASSIGN:o assignment_expr:y	{: RESULT = new BinaryExpr(pos(oleft, oright), x, BinaryOp.ASSIGN, y); :}
                  ;

logical_or_expr ::= logical_and_expr:e	{: RESULT = e; :}
                  | logical_or_expr:x OR:o logical_and_expr:y	{: RESULT = new BinaryExpr(pos(oleft, oright), x, BinaryOp.OR, y); :}
                  ;

logical_and_expr ::= equality_expr:e	{: RESULT = e; :}
                   | logical_and_expr:x AND:o equality_expr:y	{: RESULT = new BinaryExpr(pos(oleft, oright), x, BinaryOp.AND, y); :}
                   ;

equality_expr ::= relational_expr:e	{: RESULT = e; :}
                | equality_expr:x EQ:o  relational_expr:y	{: RESULT = new BinaryExpr(pos(oleft, oright), x, BinaryOp.EQ, y); :}
                | equality_expr:x NEQ:o relational_expr:y	{: RESULT = new BinaryExpr(pos(oleft, oright), x, BinaryOp.NEQ, y); :}
                ;

relational_expr ::= additive_expr:e	{: RESULT = e; :}
                  | relational_expr:x LESS:o       additive_expr:y	{: RESULT = new BinaryExpr(pos(oleft, oright), x, BinaryOp.LESS, y); :}
                  | relational_expr:x LESS_EQ:o    additive_expr:y	{: RESULT = new BinaryExpr(pos(oleft, oright), x, BinaryOp.LESS_EQ, y); :}
                  | relational_expr:x GREATER:o    additive_expr:y	{: RESULT = new BinaryExpr(pos(oleft, oright), x, BinaryOp.GREATER, y); :}
                  | relational_expr:x GREATER_EQ:o additive_expr:y	{: RESULT = new BinaryExpr(pos(oleft, oright), x, BinaryOp.GREATER_EQ, y); :}
                  ;

additive_expr ::= mult_expr:e	{: RESULT = e; :}
                | additive_expr:x PLUS:p  mult_expr:y	{: RESULT = new BinaryExpr(pos(pleft, pright), x, BinaryOp.PLUS, y); :}
                | additive_expr:x MINUS:m mult_expr:y	{: RESULT = new BinaryExpr(pos(mleft, mright), x, BinaryOp.MINUS, y); :}
                ;

mult_expr ::= unary_expr:e	{: RESULT = e; :}
            | mult_expr:x MULTIPLY:m unary_expr:y	{: RESULT = new BinaryExpr(pos(mleft, mright), x, BinaryOp.MULTIPLY, y); :}
            | mult_expr:x  DIVIDE:d  unary_expr:y	{: RESULT = new BinaryExpr(pos(dleft, dright), x, BinaryOp.DIVIDE, y); :}
            | mult_expr:x  MODULO:m  unary_expr:y	{: RESULT = new BinaryExpr(pos(mleft, mright), x, BinaryOp.MODULO, y); :}
            ;

unary_expr ::= postfix:p	{: RESULT = p; :}
             | PLUS:p  unary_expr:e	{: RESULT = new UnaryExpr(pos(pleft, pright), UnaryOp.PLUS, e); :}
             | MINUS:m unary_expr:e	{: RESULT = new UnaryExpr(pos(mleft, mright), UnaryOp.MINUS, e); :}
             | NOT:n   unary_expr:e	{: RESULT = new UnaryExpr(pos(nleft, nright), UnaryOp.NOT, e); :}
             ;

postfix ::= primary:p	{: RESULT = p; :}
          | postfix:p LBRACKET expr:e RBRACKET	{: RESULT = new SubscriptPostfix(pos(pleft, pright), p, e); :}
          | postfix:p LPAREN expr:e RPAREN	{: RESULT = new FunctionCall(pos(pleft, pright), p, e); :}
          | postfix:p LPAREN        RPAREN	{: RESULT = new FunctionCall(pos(pleft, pright), p, null); :}
          | postfix:p DOT ID:i	{: RESULT = new FieldPostfix(pos(pleft, pright), p, sym(i)); :}
          ;

primary ::= ID:i	{: RESULT = new Id(pos(ileft, iright), sym(i)); :}
          | NULL:n	{: RESULT = new Null(pos(nleft, nright)); :}
          | INTEGER:i	{: RESULT = new IntLiteral(pos(ileft, iright), i); :}
          | CHARACTER:c	{: RESULT = new CharLiteral(pos(cleft, cright), c); :}
          | STRING_LITERAL:s	{: RESULT = new StringLiteral(pos(sleft, sright), s); :}
          | LPAREN expr:e RPAREN	{: RESULT = e; :}
          | NEW:n type_specifier:t LBRACKET expr:e RBRACKET	{: RESULT = new NewArray(pos(nleft, nright), t, e); :}
          | NEW:n type_specifier:t	{: RESULT = new NewRecord(pos(nleft, nright), t); :}
          ;
