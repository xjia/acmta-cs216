package javac.parser;

%%

%unicode
%line
%column
%cup
%implements Symbols

%{
	private StringBuffer str = new StringBuffer();
	private int commentCount = 0;
	
	private void err(String message) {
		System.err.println(String.format("Scanning error in line %d, column %d: %s", yyline + 1, yycolumn + 1, message));
	}
	
	private java_cup.runtime.Symbol tok(int kind) {
		return new java_cup.runtime.Symbol(kind, yyline, yycolumn);
	}
	
	private java_cup.runtime.Symbol tok(int kind, Object value) {
		return new java_cup.runtime.Symbol(kind, yyline, yycolumn, value);
	}
%}

%eofval{
	{
		if (yystate() == YYSTRING) {
			err("String representation error (EOF)!");
		}
		if (yystate() == YYCOMMENT) {
			err("Comment symbol do not match (EOF)!");
		}
		return tok(EOF, null);
	}
%eofval}

LineTerm = \n|\r|\r\n
Identifier = [_a-zA-Z][_a-zA-Z0-9]*
DecInteger = [0-9]+
Whitespace = {LineTerm}|[ \t\f]

%state YYSTRING
%state YYCHAR
%state YYCOMMENT
%state YYLINECOMMENT

%%

<YYINITIAL> {
	\"		{ str.setLength(0); yybegin(YYSTRING); }
	\'		{ str.setLength(0); yybegin(YYCHAR); }
	"//"	{ yybegin(YYLINECOMMENT); }
	"/*"	{ commentCount = 1; yybegin(YYCOMMENT); }
	"*/"	{ err("Comment symbol do not match!"); }
	
	"native"	{ return tok(NATIVE); }
	"record"	{ return tok(RECORD); }
	"new"		{ return tok(NEW); }
	"int"		{ return tok(INT); }
	"string"	{ return tok(STRING); }
	"char"		{ return tok(CHAR); }
	"null"		{ return tok(NULL); }
	"if"		{ return tok(IF); }
	"else"		{ return tok(ELSE); }
	"while"		{ return tok(WHILE); }
	"for"		{ return tok(FOR); }
	"return"	{ return tok(RETURN); }
	"break"		{ return tok(BREAK); }
	"continue"	{ return tok(CONTINUE); }
	
	"["{Whitespace}*"]"	{ return tok(LRBRACKET); }
	
	";"		{ return tok(SEMICOLON); }
	"{"		{ return tok(LBRACE); }
	"}"		{ return tok(RBRACE); }
	"("		{ return tok(LPAREN); }
	")"		{ return tok(RPAREN); }
	","		{ return tok(COMMA); }
	"["		{ return tok(LBRACKET); }
	"]"		{ return tok(RBRACKET); }
	"="		{ return tok(ASSIGN); }
	"||"	{ return tok(OR); }
	"&&"	{ return tok(AND); }
	"=="	{ return tok(EQ); }
	"!="	{ return tok(NEQ); }
	"<"		{ return tok(LESS); }
	"<="	{ return tok(LESS_EQ); }
	">"		{ return tok(GREATER); }
	">="	{ return tok(GREATER_EQ); }
	"+"		{ return tok(PLUS); }
	"-"		{ return tok(MINUS); }
	"*"		{ return tok(MULTIPLY); }
	"/"		{ return tok(DIVIDE); }
	"%"		{ return tok(MODULO); }
	"!"		{ return tok(NOT); }
	"."		{ return tok(DOT); }
	
	{Identifier}	{ return tok(ID, yytext()); }
	{DecInteger}	{ return tok(INTEGER, Integer.valueOf(yytext())); }
	{Whitespace}	{}
	
	[^]	{ err("Illegal character " + yytext()); }
}

<YYLINECOMMENT> {
	{LineTerm}	{ yybegin(YYINITIAL); }
	[^]			{}
}

<YYCOMMENT> {
	"/*"	{ commentCount++; }
	"*/"	{ commentCount--; if (commentCount == 0) { yybegin(YYINITIAL); } }
	[^]		{}
}

<YYSTRING> {
	\"	{ yybegin(YYINITIAL); return tok(STRING_LITERAL, str.toString()); }
	\\[0-9]{3}	{
		final int n = Integer.valueOf(yytext().substring(1, 4));
		if (n > 255) {
			err("String representation error (\\ddd exceeds 255)!");
		} else {
			str.append((char) n);
		}
	}
	[^\r\n\t\"\\]+	{ str.append(yytext()); }
	\\t		{ str.append('\t'); }
	\\r 	{ str.append('\r'); }
	\\n 	{ str.append('\n'); }
	\\\"	{ str.append('\"'); }
	\\\\	{ str.append('\\'); }
	{LineTerm}	{ err("String representation error (unexpected line terminator)!"); }
}

<YYCHAR> {
	\'	{
		if (str.length() < 1) {
			err("Character representation error (too short)!");
		} else if (str.length() > 1) {
			err("Character representation error (too long)!");
		} else {
			yybegin(YYINITIAL);
			return tok(CHARACTER, str.charAt(0));
		}
	}
	\\[0-9]{3}	{
		final int n = Integer.valueOf(yytext().substring(1, 4));
		if (n > 255) {
			err("Character representation error (\\ddd exceeds 255)!");
		} else {
			str.append((char) n);
		}
	}
	[^\r\n\t\'\\]+	{ str.append(yytext()); }
	\\t		{ str.append('\t'); }
	\\r 	{ str.append('\r'); }
	\\n 	{ str.append('\n'); }
	\\\'	{ str.append('\''); }
	\\\\	{ str.append('\\'); }
	{LineTerm}	{ err("Character representation error (unexpected line terminator)!"); }
}
