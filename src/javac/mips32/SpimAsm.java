package javac.mips32;

import javac.translate.DataFrag;

import java.io.BufferedWriter;
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.List;
import java.util.LinkedList;
import java.util.Map;

public class SpimAsm {
    List<LabeledInstruction> instructions;

    List<DataFrag> strings;

    Map regMap;

    public SpimAsm() {
        instructions = new LinkedList<LabeledInstruction>();
        strings = new LinkedList<DataFrag>();
        regMap = null;
    }

    public void addStringConst(DataFrag frag) {
        strings.add(frag);
    }

    public void output(BufferedWriter writer) throws IOException {
        writeTextSeg(writer);
        writer.newLine();
        writer.newLine();
        writeDataSeg(writer);
    }

    private void writeTextSeg(BufferedWriter writer) throws IOException {
        writer.write("\t.text");
        writer.newLine();
        writer.write("\t.align 2");
        writer.newLine();
        writer.write("\t.globl _main");
        writer.newLine();
        writer.write("_main:");
        writer.newLine();
        writer.write("\tjal main");
        writer.newLine();
        writer.write("\tjr $ra");
        writer.newLine();

        for (LabeledInstruction li: instructions) {
            if (li.label != null) {
                writer.write(li.label + ":");
                writer.newLine();
            }
            if (li.instruction != null) {
                writer.write("\t" + li.instruction.toString(regMap));
                writer.newLine();
            }
        }

        BufferedReader reader = new BufferedReader(new FileReader("runtime.s"));
        String s = null;
        while ((s = reader.readLine()) != null) {
            writer.write(s);
            writer.newLine();
        }
    }

    private void writeDataSeg(BufferedWriter writer) throws IOException {
        writer.write("\t.data");
        writer.newLine();
        writer.write("\t.align 2");
        writer.newLine();

        for (DataFrag frag: strings) {
            writer.write(frag.label + ":");
            writer.newLine();
            writer.write("\t.asciiz \"" + escape(frag.data) + "\"");
            writer.newLine();
        }
    }

    private String escape(String s) {
        StringBuffer ret = new StringBuffer();
        for (int i = 0; i < s.length(); ++i) {
            char c = s.charAt(i);
            if (c == '\t')
                ret.append("\\t");
            else if (c == '\n')
                ret.append("\\n");
            else if (c == '\\')
                ret.append("\\\\");
            else
                ret.append(c);
        }
        return ret.toString();
    }

    public void setRegMap(Map map) {
        regMap = map;
    }
}
