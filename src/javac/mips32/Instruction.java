package javac.mips32;

import javac.quad.Temp;
import javac.quad.Label;
import java.util.Map;
import java.util.HashMap;

public class Instruction {
    Type type;
    Temp dst = null, src1 = null, src2 = null;
    int imm = 0;
    Label target = null;

    static enum Type {
        MOVE, ADD, ADDI, ADDIU,
            SUB, MUL,
            DIV, REM, NEG,
            SLL, SRL,
            SLT, SLTI,
            SLE, SEQ,
            SNE, SGT, SGE,
            LW, LB,
            SW, SB, J,
            JAL, JR,
            BEQ, BNE,
            BLT, BGT,
            BLE, BGE,
            LI, LA, SYSCALL
    }

    public Instruction(Type type, Temp dst, Temp src1, Temp src2, int imm, Label target) {
        this.type = type;
        this.dst = dst;
        this.src1 = src1;
        this.src2 = src2;
        this.imm = imm;
        this.target = target;
    }

    public static Instruction MOVE(Temp dst, Temp src) {
        return new Instruction(Type.MOVE, dst, src, null, 0, null);
    }

    public static Instruction ADD(Temp dst, Temp src1, Temp src2) {
        return new Instruction(Type.ADD, dst, src1, src2, 0, null);
    }

    public static Instruction ADDI(Temp dst, Temp src1, int src2) {
        return new Instruction(Type.ADDI, dst, src1, null, src2, null);
    }

    public static Instruction ADDIU(Temp dst, Temp src1, int src2) {
        return new Instruction(Type.ADDIU, dst, src1, null, src2, null);
    }

    public static Instruction SUB(Temp dst, Temp src1, Temp src2) {
        return new Instruction(Type.SUB, dst, src1, src2, 0, null);
    }

    public static Instruction MUL(Temp dst, Temp src1, Temp src2) {
        return new Instruction(Type.MUL, dst, src1, src2, 0, null);
    }

    public static Instruction DIV(Temp dst, Temp src1, Temp src2) {
        return new Instruction(Type.DIV, dst, src1, src2, 0, null);
    }

    public static Instruction REM(Temp dst, Temp src1, Temp src2) {
        return new Instruction(Type.REM, dst, src1, src2, 0, null);
    }

    public static Instruction SLL(Temp dst, Temp src1, int src2) {
        return new Instruction(Type.SLL, dst, src1, null, src2, null);
    }

    public static Instruction SRL(Temp dst, Temp src1, int src2) {
        return new Instruction(Type.SRL, dst, src1, null, src2, null);
    }

    public static Instruction NEG(Temp dst, Temp src) {
        return new Instruction(Type.NEG, dst, src, null, 0, null);
    }

    public static Instruction SLT(Temp dst, Temp src1, Temp src2) {
        return new Instruction(Type.SLT, dst, src1, src2, 0, null);
    }

    public static Instruction SLTI(Temp dst, Temp src1, int src2) {
        return new Instruction(Type.SLTI, dst, src1, null, src2, null);
    }

    public static Instruction SLE(Temp dst, Temp src1, Temp src2) {
        return new Instruction(Type.SLE, dst, src1, src2, 0, null);
    }

    public static Instruction SEQ(Temp dst, Temp src1, Temp src2) {
        return new Instruction(Type.SEQ, dst, src1, src2, 0, null);
    }

    public static Instruction SNE(Temp dst, Temp src1, Temp src2) {
        return new Instruction(Type.SNE, dst, src1, src2, 0, null);
    }

    public static Instruction SGT(Temp dst, Temp src1, Temp src2) {
        return new Instruction(Type.SGT, dst, src1, src2, 0, null);
    }

    public static Instruction SGE(Temp dst, Temp src1, Temp src2) {
        return new Instruction(Type.SGE, dst, src1, src2, 0, null);
    }

    public static Instruction LW(Temp dst, Temp base, int offset) {
        return new Instruction(Type.LW, dst, base, null, offset, null);
    }

    public static Instruction LB(Temp dst, Temp base, int offset) {
        return new Instruction(Type.LB, dst, base, null, offset, null);
    }

    public static Instruction SW(Temp value, Temp base, int offset) {
        return new Instruction(Type.SW, null, base, value, offset, null);
    }

    public static Instruction SB(Temp value, Temp base, int offset) {
        return new Instruction(Type.SB, null, base, value, offset, null);
    }

    public static Instruction J(Label target) {
        return new Instruction(Type.J, null, null, null, 0, target);
    }

    public static Instruction JAL(Label target) {
        return new Instruction(Type.JAL, null, null, null, 0, target);
    }

    public static Instruction JR(Temp src) {
        return new Instruction(Type.JR, null, src, null, 0, null);
    }

    public static Instruction BEQ(Temp src1, Temp src2, Label target) {
        return new Instruction(Type.BEQ, null, src1, src2, 0, target);
    }

    public static Instruction BNE(Temp src1, Temp src2, Label target) {
        return new Instruction(Type.BNE, null, src1, src2, 0, target);
    }

    public static Instruction BLT(Temp src1, Temp src2, Label target) {
        return new Instruction(Type.BLT, null, src1, src2, 0, target);
    }
    
    public static Instruction BLE(Temp src1, Temp src2, Label target) {
        return new Instruction(Type.BLE, null, src1, src2, 0, target);
    }

    public static Instruction BGT(Temp src1, Temp src2, Label target) {
        return new Instruction(Type.BGT, null, src1, src2, 0, target);
    }

    public static Instruction BGE(Temp src1, Temp src2, Label target) {
        return new Instruction(Type.BGE, null, src1, src2, 0, target);
    }

    public static Instruction LI(Temp dst, int imm) {
        return new Instruction(Type.LI, dst, null, null, imm, null);
    }

    public static Instruction LA(Temp dst, Label target) {
        return new Instruction(Type.LA, dst, null, null, 0, target);
    }

    public static Instruction SYSCALL() {
        return new Instruction(Type.SYSCALL, null, null, null, 0, null);
    }

    public String toString() {
        Map<Temp, Temp> map = new HashMap<Temp, Temp>();
        if (src1 != null)
            map.put(src1, src1);
        if (src2 != null)
            map.put(src2, src2);
        if (dst != null)
            map.put(dst, dst);
        return toString(map);
    }

    public String toString(Map map) {
        if (map == null)
            return toString();
        String s = "";
        switch (type) {
            case MOVE:
                s = "move " + map.get(dst).toString() + ", " + map.get(src1).toString();
                break;

            case ADD:
                s = "add " + map.get(dst).toString() + ", " + map.get(src1).toString() + ", " + map.get(src2).toString();
                break;

            case ADDI:
                s = "addi " + map.get(dst).toString() + ", " + map.get(src1).toString() + ", " + imm;
                break;

            case ADDIU:
                s = "addiu " + map.get(dst).toString() + ", " + map.get(src1).toString() + ", " + imm;
                break;

            case SUB:
                s = "sub " + map.get(dst).toString() + ", " + map.get(src1).toString() + ", " + map.get(src2).toString();
                break;

            case MUL:
                s = "mul " + map.get(dst).toString() + ", " + map.get(src1).toString() + ", " + map.get(src2).toString();
                break;

            case DIV:
                s = "div " + map.get(dst).toString() + ", " + map.get(src1).toString() + ", " + map.get(src2).toString();
                break;

            case REM:
                s = "rem " + map.get(dst).toString() + ", " + map.get(src1).toString() + ", " + map.get(src2).toString();
                break;

            case SLL:
                s = "sll " + map.get(dst).toString() + ", " + map.get(src1).toString() + ", " + imm;
                break;

            case SRL:
                s = "srl " + map.get(dst).toString() + ", " + map.get(src1).toString() + ", " + imm;
                break;

            case NEG:
                s = "neg " + map.get(dst).toString() + ", " + map.get(src1).toString();
                break;

            case SLT:
                s = "slt " + map.get(dst).toString() + ", " + map.get(src1).toString() + ", " + map.get(src2).toString();
                break;

            case SLTI:
                s = "slti " + map.get(dst).toString() + ", " + map.get(src1).toString() + ", " + imm;
                break;
                
            case SLE:
                s = "sle " + map.get(dst).toString() + ", " + map.get(src1).toString() + ", " + map.get(src2).toString();
                break;

            case SEQ:
                s = "seq " + map.get(dst).toString() + ", " + map.get(src1).toString() + ", " + map.get(src2).toString();
                break;

            case SNE:
                s = "sne " + map.get(dst).toString() + ", " + map.get(src1).toString() + ", " + map.get(src2).toString();
                break;

            case SGT:
                s = "sgt " + map.get(dst).toString() + ", " + map.get(src1).toString() + ", " + map.get(src2).toString();
                break;

            case SGE:
                s = "sge " + map.get(dst).toString() + ", " + map.get(src1).toString() + ", " + map.get(src2).toString();
                break;

            case J:
                s = "j " + target.toString();
                break;

            case JAL:
                s = "jal " + target.toString();
                break;

            case JR:
                s = "jr " + map.get(src1).toString();
                break;

            case LW:
                s += "lw " + map.get(dst).toString() + ", " + imm + "(" + map.get(src1).toString() + ")";
                break;

            case LB:
                s = "lb " + map.get(dst).toString() + ", " + imm + "(" + map.get(src1).toString() + ")";
                break;

            case SW:
                s = "sw " + map.get(src2).toString() + ", " + imm + "(" + map.get(src1).toString() + ")";
                break;

            case SB:
                s = "sb " + map.get(src2).toString() + ", " + imm + "(" + map.get(src1).toString() + ")";
                break;

            case BEQ:
                s = "beq " + map.get(src1).toString() + ", " + map.get(src2).toString() + ", " + target.toString();
                break;

            case BNE:
                s = "bne " + map.get(src1).toString() + ", " + map.get(src2).toString() + ", " + target.toString();
                break;

            case BLT:
                s = "blt " + map.get(src1).toString() + ", " + map.get(src2).toString() + ", " + target.toString();
                break;

            case BLE:
                s = "ble " + map.get(src1).toString() + ", " + map.get(src2).toString() + ", " + target.toString();
                break;

            case BGT:
                s = "bgt " + map.get(src1).toString() + ", " + map.get(src2).toString() + ", " + target.toString();
                break;

            case BGE:
                s = "bge " + map.get(src1).toString() + ", " + map.get(src2).toString() + ", " + target.toString();
                break;

            case LI:
                s = "li " + map.get(dst).toString() + ", " + imm;
                break;

            case LA:
                s = "la " + map.get(dst).toString() + ", " + target.toString();
                break;

            case SYSCALL:
                s = "syscall";
        }
        return s;
    }
}
