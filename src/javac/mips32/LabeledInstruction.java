package javac.mips32;

import javac.quad.Label;

public class LabeledInstruction {
    Label label;
    Instruction instruction;

    public LabeledInstruction(Label label) {
        this.label = label;
        this.instruction = null;
    }

    public LabeledInstruction(Instruction instruction) {
        this.instruction = instruction;
        this.label = null;
    }
}
