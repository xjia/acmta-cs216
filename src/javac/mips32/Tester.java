package javac.mips32;

import java.io.FileInputStream;
import java.io.InputStream;
import java.io.BufferedWriter;
import java.io.FileWriter;
import java.util.List;

import javac.translate.Frag;
import javac.translate.Translator;
import javac.absyn.TranslationUnit;
import javac.parser.Parser;
import javac.semant.FuncSemant;
import javac.semant.GlobalEnvVisitor;
import javac.semant.RecordSemant;
import javac.semant.Semant;

public class Tester {
    public static final void main(String[] args) throws Exception {
        String fileName = args[0];

        final InputStream in = new FileInputStream(fileName);
        final Parser parser = new Parser(in);
        final java_cup.runtime.Symbol parseTree = parser.parse();
        in.close();
        final TranslationUnit translationUnit = (TranslationUnit) parseTree.value;
        GlobalEnvVisitor gev = new GlobalEnvVisitor();
        translationUnit.accept(gev);
        Semant recordSemant = new RecordSemant(gev.getGlobalEnv());
        translationUnit.accept(recordSemant);
        Semant funcSemant = new FuncSemant(recordSemant.getEnv());

        translationUnit.accept(funcSemant);

        CodeGen codeGen = new CodeGen();
        List<Frag> ir = Translator.Translate(translationUnit);
        for (Frag f: ir) {
            System.out.println(f);
        }
        SpimAsm asm = codeGen.codeGen(ir);

        BufferedWriter writer = new BufferedWriter(new FileWriter("test/a.s"));
        asm.output(writer);
        writer.close();
    }
}
