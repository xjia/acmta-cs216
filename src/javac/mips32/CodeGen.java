package javac.mips32;

import javac.translate.*;
import javac.quad.*;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

public class CodeGen {
    private static Temp zero = new Temp(), v0 = new Temp(), ra = new Temp(), fp = new Temp(), sp = new Temp(),
            t0 = new Temp(), t1 = new Temp(), t2 = new Temp();
    public static int wordLength = FuncTranslator.wordSize;

    public SpimAsm codeGen(List<Frag> ir) throws Exception {
        SpimAsm asm = new SpimAsm();
        for (Frag f: ir)
            processFrag(f, asm);
        
        Map<Temp, String> map = new HashMap<Temp, String>();
        map.put(zero, "$zero");
        map.put(v0, "$v0");
        map.put(ra, "$ra");
        map.put(fp, "$fp");
        map.put(sp, "$sp");
        map.put(t0, "$t0");
        map.put(t1, "$t1");
        map.put(t2, "$t2");
        asm.setRegMap(map);

        return asm;
    }

    private boolean isRegTemp(Temp t) {
        if (t == zero || t == v0 || t == ra || t == fp || t == sp
                || t == t0 || t == t1 || t == t2)
            return true;
        else
            return false;
    }

    private void processFrag(Frag frag, SpimAsm asm) throws Exception {
        if (frag instanceof FuncFrag)
            processFrag((FuncFrag) frag, asm);
        else if (frag instanceof DataFrag)
            processFrag((DataFrag) frag, asm);
    }

    private void onEnterFunction(FuncFrag frag, SpimAsm asm, int stackSize) throws Exception {
        List<LabeledInstruction> instructions = asm.instructions;

        /* Save $ra and $fp */
        instructions.add(new LabeledInstruction(Instruction.SW(ra, sp, 0)));
        instructions.add(new LabeledInstruction(Instruction.SW(fp, sp, -wordLength)));
        /* Set new $fp */
        instructions.add(new LabeledInstruction(Instruction.MOVE(fp, sp)));
        /* Set new $sp */
        instructions.add(new LabeledInstruction(Instruction.ADDI(sp, sp, -stackSize)));
    }

    private void onLeaveFunction(List<LabeledInstruction> instructions, int stackSize) {
        /* Restore $ra and $fp */
        instructions.add(new LabeledInstruction(Instruction.LW(ra, fp, 0)));
        instructions.add(new LabeledInstruction(Instruction.LW(fp, fp, -wordLength)));
        /* Balance stack */
        instructions.add(new LabeledInstruction(Instruction.ADDI(sp, sp, stackSize)));
    }

    private class SpillInfo {
        Map<Temp, Integer> spills;
        int spillCount;

        public SpillInfo() {
            spills = new HashMap<Temp, Integer>();
            spillCount = 0;
        }

        public int get(Temp t) {
            Integer i = spills.get(t);
            if (i == null) {
                i = new Integer(-(spillCount + 2) * wordLength);
                spills.put(t, i);
                ++spillCount;
                return i.intValue();
            } else
                return i.intValue();
        }

        public void put(Temp t, int offset) {
            if (!spills.containsKey(t))
                spills.put(t, new Integer(offset));
        }
    }

    private void spill(Instruction ins, SpillInfo si, List<LabeledInstruction> ret) {
        Temp src1 = ins.src1, src2 = ins.src2, dst = ins.dst;
        if (src1 != null && !isRegTemp(src1)) {
            int offset = si.get(src1);
            ret.add(new LabeledInstruction(Instruction.LW(t0, fp, offset)));
            src1 = t0;
        }
        if (src2 != null && !isRegTemp(src2)) {
            int offset = si.get(src2);
            ret.add(new LabeledInstruction(Instruction.LW(t1, fp, offset)));
            src2 = t1;
        }

        boolean saveToDst = false;
        int dstOffset = 0;
        if (dst != null && !isRegTemp(dst)) {
            saveToDst = true;
            dstOffset = si.get(dst);
            dst = t2;
        }

        ret.add(new LabeledInstruction(new Instruction(ins.type, dst, src1, src2, ins.imm, ins.target)));

        if (saveToDst)
            ret.add(new LabeledInstruction(Instruction.SW(dst, fp, dstOffset)));
    }

    private void processFrag(FuncFrag frag, SpimAsm asm) throws Exception {
        Label leaveLabel = new Label("__" + frag.function.toString());
        List<LabeledInstruction> ret = new LinkedList<LabeledInstruction>();
        for (Quad q: frag.body)
            processQuad(q, ret, leaveLabel);

        List<LabeledInstruction> spilled = new LinkedList<LabeledInstruction>();
        SpillInfo si = new SpillInfo();
        int nParams = frag.params.length;
        for (int i = 0; i < frag.params.length; ++i)
            si.put(frag.params[i], (nParams - i) * wordLength);
        for (LabeledInstruction li: ret) {
            if (li.label != null)
                spilled.add(new LabeledInstruction(li.label));
            if (li.instruction != null)
                spill(li.instruction, si, spilled);
        }


        List<LabeledInstruction> instructions = asm.instructions;
        instructions.add(new LabeledInstruction(frag.function));

        onEnterFunction(frag, asm, (si.spillCount + 2) * wordLength);

        instructions.addAll(spilled);

        instructions.add(new LabeledInstruction(leaveLabel));
        onLeaveFunction(instructions, (si.spillCount + 2) * wordLength);
        instructions.add(new LabeledInstruction(Instruction.JR(ra)));
    }

    private void processFrag(DataFrag frag, SpimAsm asm) {
        asm.addStringConst(frag);
    }

    private void processQuad(Quad quad, List<LabeledInstruction> ret, Label leaveLabel) throws Exception {
        if (quad instanceof BinOp)
            processQuad((BinOp) quad, ret);
        else if (quad instanceof Call)
            processQuad((Call) quad, ret);
        else if (quad instanceof Jump)
            processQuad((Jump) quad, ret);
        else if (quad instanceof Return)
            processQuad((Return) quad, ret, leaveLabel);
        else if (quad instanceof Branch)
            processQuad((Branch) quad, ret);
        else if (quad instanceof LabelQuad)
            processQuad((LabelQuad) quad, ret);
        else if (quad instanceof Move)
            processQuad((Move) quad, ret);
    }

    private void processQuad(BinOp quad, List<LabeledInstruction> ret) throws Exception {
        Temp op1 = processOprand(quad.left, ret), op2 = processOprand(quad.right, ret);
        Temp t = new Temp();
        switch (quad.op) {
            case PLUS:
                ret.add(new LabeledInstruction(Instruction.ADD(t, op1, op2)));
                break;

            case MINUS:
                ret.add(new LabeledInstruction(Instruction.SUB(t, op1, op2)));
                break;

            case MULTIPLY:
                ret.add(new LabeledInstruction(Instruction.MUL(t, op1, op2)));
                break;

            case DIVIDE:
                ret.add(new LabeledInstruction(Instruction.DIV(t, op1, op2)));
                break;

            case MODULO:
                ret.add(new LabeledInstruction(Instruction.REM(t, op1, op2)));
                break;

            case LESS:
                ret.add(new LabeledInstruction(Instruction.SLT(t, op1, op2)));
                break;

            case LESS_EQ:
                ret.add(new LabeledInstruction(Instruction.SLE(t, op1, op2)));
                break;

            case GREATER:
                ret.add(new LabeledInstruction(Instruction.SGT(t, op1, op2)));
                break;

            case GREATER_EQ:
                ret.add(new LabeledInstruction(Instruction.SGE(t, op1, op2)));
                break;

            case EQ:
                ret.add(new LabeledInstruction(Instruction.SEQ(t, op1, op2)));
                break;

            case NEQ:
                ret.add(new LabeledInstruction(Instruction.SNE(t, op1, op2)));
                break;

            case AND:
            case OR:
            default:
                throw new Exception("Unsupported binary op");
        }
        
        saveToDest(quad.dst, t, ret);
    }

    private void processQuad(Call quad, List<LabeledInstruction> ret) {
        int nParams = quad.params.length;
        for (int i = 0; i < quad.params.length; ++i)
            ret.add(new LabeledInstruction(Instruction.SW(quad.params[i], sp, -i * wordLength)));
        ret.add(new LabeledInstruction(Instruction.ADDI(sp, sp, -nParams * wordLength)));

        ret.add(new LabeledInstruction(Instruction.JAL(quad.function)));

        ret.add(new LabeledInstruction(Instruction.ADDI(sp, sp, nParams * wordLength)));
        ret.add(new LabeledInstruction(Instruction.MOVE(quad.result, v0)));
    }

    private void processQuad(Jump quad, List<LabeledInstruction> ret) {
        ret.add(new LabeledInstruction(Instruction.J(quad.label)));
    }

    private void processQuad(Return quad, List<LabeledInstruction> ret, Label leaveLabel) throws Exception {
        Temp t = processOprand(quad.value, ret);
        ret.add(new LabeledInstruction(Instruction.MOVE(v0, t)));

        ret.add(new LabeledInstruction(Instruction.J(leaveLabel)));
    }

    private void processQuad(Branch quad, List<LabeledInstruction> ret) throws Exception {
        Temp op1 = processOprand(quad.left, ret), op2 = processOprand(quad.right, ret);
        switch (quad.op) {
            case EQ:
                ret.add(new LabeledInstruction(Instruction.BEQ(op1, op2, quad.label)));
                break;

            case NEQ:
                ret.add(new LabeledInstruction(Instruction.BNE(op1, op2, quad.label)));
                break;

            case LESS:
                ret.add(new LabeledInstruction(Instruction.BLT(op1, op2, quad.label)));
                break;

            case LESS_EQ:
                ret.add(new LabeledInstruction(Instruction.BLE(op1, op2, quad.label)));
                break;

            case GREATER:
                ret.add(new LabeledInstruction(Instruction.BGT(op1, op2, quad.label)));
                break;

            case GREATER_EQ:
                ret.add(new LabeledInstruction(Instruction.BGE(op1, op2, quad.label)));
                break;

            default:
                throw new Exception("Unsupported branch operation: " + quad.op);
        }
    }

    private void processQuad(LabelQuad quad, List<LabeledInstruction> ret) {
        ret.add(new LabeledInstruction(quad.label));
    }

    private void processQuad(Move quad, List<LabeledInstruction> ret) throws Exception {
        Temp src = processOprand(quad.src, ret), dst = new Temp();
        ret.add(new LabeledInstruction(Instruction.MOVE(dst, src)));
        saveToDest(quad.dst, dst, ret);
    }

    private Temp processOprand(Oprand oprand, List<LabeledInstruction> ret) throws Exception {
        if (oprand instanceof TempOprand)
            return processOprand((TempOprand) oprand, ret);
        else if (oprand instanceof Mem)
            return processOprand((Mem) oprand, ret);
        else if (oprand instanceof Const)
            return processOprand((Const) oprand, ret);
        else if (oprand instanceof LabelAddress)
            return processOprand((LabelAddress) oprand, ret);
        else
            throw new Exception("Unknown oprand type");
    }

    private Temp processOprand(TempOprand oprand, List<LabeledInstruction> ret) {
        return oprand.temp;
    }

    private Temp processOprand(Mem oprand, List<LabeledInstruction> ret) throws Exception {
        Temp base = oprand.base;
        if (base == null)
            base = zero;

        Temp r = new Temp();
        if (oprand.length == 4) {
            LabeledInstruction li = new LabeledInstruction(Instruction.LW(r, base, oprand.offset));
            ret.add(li);
            return r;
        } else if (oprand.length == 1) {
            LabeledInstruction li = new LabeledInstruction(Instruction.LB(r, base, oprand.offset));
            ret.add(li);
            return r;
        } else
            throw new Exception("Invalid mem length");
    }

    private Temp processOprand(Const oprand, List<LabeledInstruction> ret) {
        Temp r = new Temp();
        LabeledInstruction li = new LabeledInstruction(Instruction.LI(r, oprand.value));
        ret.add(li);
        return r;
    }

    private Temp processOprand(LabelAddress oprand, List<LabeledInstruction> ret) {
        Temp r = new Temp();
        LabeledInstruction li = new LabeledInstruction(Instruction.LA(r, oprand.label));
        ret.add(li);
        return r;
    }

    private void saveToDest(Oprand dest, Temp src, List<LabeledInstruction> ret) throws Exception {
        if (dest instanceof TempOprand)
            saveToDest((TempOprand) dest, src, ret);
        else if (dest instanceof Mem)
            saveToDest((Mem) dest, src, ret);
        else
            throw new Exception("Destination of type " + dest.getClass().toString() + " is readonly");
    }

    private void saveToDest(TempOprand dest, Temp src, List<LabeledInstruction> ret) {
        ret.add(new LabeledInstruction(Instruction.MOVE(dest.temp, src)));
    }

    private void saveToDest(Mem dest, Temp src, List<LabeledInstruction> ret) throws Exception {
        Temp base = dest.base;
        if (base == null)
            base = zero;

        if (dest.length == 4) {
            LabeledInstruction li = new LabeledInstruction(Instruction.SW(src, base, dest.offset));
            ret.add(li);
        } else if (dest.length == 1) {
            LabeledInstruction li = new LabeledInstruction(Instruction.SB(src, base, dest.offset));
            ret.add(li);
        } else
            throw new Exception("Invalid mem length");
   }
}
