package javac.semant;

public class SemantException extends RuntimeException {

	private static final long serialVersionUID = 5466091932671922040L;

	public SemantException(String desp) {
		super(desp);
	}
}
