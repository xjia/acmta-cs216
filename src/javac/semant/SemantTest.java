package javac.semant;

import java.io.FileInputStream;
import java.io.InputStream;

import javac.absyn.TranslationUnit;
import javac.parser.Parser;

public class SemantTest {

	public static void main(String[] args) throws Exception {
		semant("record1.java");
	}

	private static void semant(String filename) throws Exception {
		final InputStream in = new FileInputStream(filename);
		final Parser parser = new Parser(in);
		final java_cup.runtime.Symbol parseTree = parser.parse();
		in.close();
		final TranslationUnit translationUnit = (TranslationUnit) parseTree.value;
		GlobalEnvVisitor gev = new GlobalEnvVisitor();
		translationUnit.accept(gev);
		Semant recordSemant = new RecordSemant(gev.getGlobalEnv());
		translationUnit.accept(recordSemant);
		Semant funcSemant = new FuncSemant(recordSemant.getEnv());
		translationUnit.accept(funcSemant);
	}

}
