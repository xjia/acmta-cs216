package javac.semant;

import java.util.HashSet;
import java.util.Set;

import javac.absyn.*;
import javac.env.Entry;
import javac.env.Env;
import javac.env.TypeEntry;
import javac.symbol.Symbol;
import javac.type.RECORD;
import javac.type.RECORD.RecordField;
import javac.type.Type;

public class RecordSemant extends Semant {

	public RecordSemant(Env env) {
		super(env);
	}

	@Override
	public void visit(RecordDef recordDef) {
		Set<Symbol> symbolSet = new HashSet<Symbol>();
		final Entry entry = env.getEntry(recordDef.name);
		final TypeEntry typeEntry = TypeEntry.transferTypeEntry(entry);
		final RECORD record = (RECORD) typeEntry.type;

		int index = 0;
		for (VariableDecl decl : recordDef.fields.variableDeclarations) {
			decl.inRecord = true;
			final Type type = decl.type.toType(env);
			for (Symbol name : decl.ids.ids) {
				if (symbolSet.contains(name))
					throw new SemantException("Duplicated Names in Record");
				symbolSet.add(name);
				record.fields.add(new RecordField(type, name, index++));
			}
		}
	}
}
