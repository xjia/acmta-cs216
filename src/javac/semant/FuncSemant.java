package javac.semant;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import javac.absyn.*;
import javac.env.Entry;
import javac.env.Env;
import javac.env.FuncEntry;
import javac.env.VarEntry;
import javac.symbol.Symbol;
import javac.type.ARRAY;
import javac.type.CHAR;
import javac.type.INT;
import javac.type.RECORD;
import javac.type.STRING;
import javac.type.Type;

public class FuncSemant extends Semant {

	Node breakOrContinue = null;

	Type mayReturn = null;

	HashMap<Symbol, Env> funcEnvs = new HashMap<Symbol, Env>();
	
	private Env globalEnv;
	
	private void exitFunc(){
		env = globalEnv.clone();
	}
	
	public Map<Symbol, Env> getFuncEnvs(){
		return funcEnvs;
	}
	
	public FuncSemant(Env env) {
		super(env);
		globalEnv = env.clone();
	}

	private static Type checkEqual(BinaryExpr expr) {
		Type left = expr.l.ty;
		Type right = expr.r.ty;

		if (!left.getClass().getSimpleName()
				.equals(right.getClass().getSimpleName()))
			throw new SemantException(
					"Two Operator on the two sides have different types.");

		return left;
	}

	private static void fuckArrayAndRecord(Type left, Type right) {
		if (left.isRecord() || right.isRecord() || left.isArray()
				|| right.isArray())
			throw new SemantException(
					"Plus Operation Cannot Used on Record or Array");
	}

	private static Type plusMaping(Type left, Type right) {
		fuckArrayAndRecord(left, right);

		if (left instanceof STRING || right instanceof STRING)
			return STRING.getInstance();

		return INT.getInstance();
	}

	private static INT checkINT(BinaryExpr expr) {
		Expr l = expr.l;
		Expr r = expr.r;
		if (l.ty instanceof INT && r.ty instanceof INT)
			return INT.getInstance();
		else
			throw new SemantException("Two Operator must be both Integer.");

	}

	@Override
	public void visit(ArrayType arrayType) {
	}

	@Override
	public void visit(BinaryExpr binaryExpr) {
		switch (binaryExpr.op) {
		case COMMA:
			binaryExpr.ty = binaryExpr.r.ty;
			break;
		case ASSIGN:
			binaryExpr.ty = checkEqual(binaryExpr);
			break;
		case OR:
		case AND:
			binaryExpr.ty = checkINT(binaryExpr);
			break;
		case EQ:
		case NEQ:
			checkEqual(binaryExpr);
			binaryExpr.ty = INT.getInstance();
			break;
		case LESS:
		case GREATER:
		case LESS_EQ:
		case GREATER_EQ:
			fuckArrayAndRecord(binaryExpr.l.ty, binaryExpr.r.ty);
			binaryExpr.ty = INT.getInstance();
			break;
		case PLUS:
			binaryExpr.ty = plusMaping(binaryExpr.l.ty, binaryExpr.r.ty);
			break;
		case MINUS:
		case MULTIPLY:
		case DIVIDE:
		case MODULO:
			binaryExpr.ty = checkINT(binaryExpr);
			break;
		default:
			throw new SemantException("No this Operation");

		}
	}

	@Override
	public void visit(BreakStmt breakStmt) {
		breakOrContinue = breakStmt;
	}

	@Override
	public void visit(CharLiteral charLiteral) {
		charLiteral.ty = CHAR.getInstance();
	}

	@Override
	public void visit(CharType charType) {
	}

	@Override
	public void visit(CompoundStmt compoundStmt) {
	}

	@Override
	public void visit(ContinueStmt continueStmt) {
		breakOrContinue = continueStmt;
	}

	@Override
	public void visit(ExprStmt exprStmt) {
	}

	@Override
	public void visit(FieldPostfix fieldPostfix) {
		Expr a = fieldPostfix.expr;
		Symbol b = fieldPostfix.field;

		if (a.ty.equals(STRING.getInstance()) || a.ty.isArray()) {
			if (!b.equals(Symbol.valueOf("length")))
				throw new SemantException(
						"field postfix must be length for string and arrays");
			fieldPostfix.ty = INT.getInstance();
		} else if (a.ty.isRecord()) {
			if (a.ty.isNull())
				throw new SemantException("fieldPostFix at a null");
			else {
				RECORD r = (RECORD) a.ty;
				if (!r.hasField(b))
					throw new SemantException("cannot find field");
				fieldPostfix.ty = r.getFieldType(b);
			}
		}
		

	}

	@Override
	public void visit(ForStmt forStmt) {

		breakOrContinue = null;

		if (!forStmt.cond.ty.equals(INT.getInstance()))
			throw new SemantException("Not a int valued condition");

	}

	@Override
	public void visit(FunctionCall functionCall) {
		if (!(functionCall.expr instanceof Id))
			throw new SemantException(
					"Now the Called Function Name must be a Id.");

		Symbol funcName = ((Id) functionCall.expr).sym;

		Entry entry = env.getEntry(funcName);

		if (!(entry instanceof FuncEntry))
			throw new SemantException(funcName + " is not a name of function.");

		FuncEntry funcEntry = (FuncEntry) entry;

		Expr args = functionCall.args;

		Expr iterator = args;

		List<ParameterDecl> declList = funcEntry.head.parameterList.parameterDeclarations;

		functionCall.params = new LinkedList<Expr>();
		
		for (int i = funcEntry.head.parameterList.parameterDeclarations.size() - 1; i >= 0; --i) {

			if (iterator == null)
				throw new SemantException(
						"No Mathced from function Declaration.");

			if (!isCommaOperator(iterator)) {
				if (!iterator.ty.equals(declList.get(i).type.toType(env)))
					throw new SemantException("Uncompatibale Parameter.");
				functionCall.params.add(0, iterator);
				iterator = null;
			} else {
				Expr right = ((BinaryExpr) iterator).r;
				if (!right.ty.equals(declList.get(i).type.toType(env)))
					throw new SemantException("Uncompatibale Parameter.");
				functionCall.params.add(0, right);
				iterator = ((BinaryExpr) iterator).l;
			}
		}

		if (iterator != null)
			throw new SemantException("No Matched from Function Declaration.");

		functionCall.ty = funcEntry.head.type.toType(env);
	}

	private boolean isCommaOperator(Expr exp) {
		if (!(exp instanceof BinaryExpr))
			return false;

		return ((BinaryExpr) exp).op.equals(BinaryOp.COMMA);
	}

	@Override
	public void visit(FunctionDef functionDef) {
		/**
		 * Check for Break or Continue at Wrong position and Check for Return a
		 * wrong Type
		 */
		if (breakOrContinue != null)
			throw new SemantException("Break or Continue at a Wrong Place");

		if (mayReturn != null
				&& !functionDef.head.type.toType(env).equals(mayReturn))
			throw new SemantException(
					"Uncompatible Type with function declaration and return value");

		mayReturn = null;
		funcEnvs.put(functionDef.head.functionName, env);
		exitFunc();
	}

	@Override
	public void visit(FunctionHead functionHead) {
		functionHead.type.toType(env);
	}

	@Override
	public void visit(Id id) {
		Entry entry = env.getEntry(id.sym);
		if (entry instanceof VarEntry) {
			VarEntry varEntry = (VarEntry) entry;
			id.ty = varEntry.type;
		} else if (entry instanceof FuncEntry) {
			FuncEntry funcEntry = (FuncEntry) entry;
			id.ty = funcEntry.head.type.toType(env);
		}
	}

	@Override
	public void visit(IdList idList) {
	}

	@Override
	public void visit(IdType idType) {
	}

	@Override
	public void visit(IfStmt ifStmt) {
		if (!ifStmt.cond.ty.equals(INT.getInstance()))
			throw new SemantException("not a int valued condition");
	}

	@Override
	public void visit(IntLiteral intLiteral) {
		intLiteral.ty = INT.getInstance();
	}

	@Override
	public void visit(IntType intType) {
	}

	@Override
	public void visit(NewArray newArray) {
		newArray.ty = new ARRAY(newArray.type.toType(env));
	}

	@Override
	public void visit(NewRecord newRecord) {
		if (!(newRecord.type instanceof IdType))
			throw new SemantException("newRecord type must be id");

		IdType type = (IdType) newRecord.type;

		newRecord.ty = type.toType(env);
	}

	@Override
	public void visit(Null n) {
		n.ty = Type.NULL;
	}

	@Override
	public void visit(ParameterDecl parameterDecl) {
		parameterDecl.type.toType(env);
	}

	@Override
	public void visit(ParameterList parameterList) {
		for (int i = 0; i < parameterList.parameterDeclarations.size(); ++i)
			for (int j = i + 1; j < parameterList.parameterDeclarations.size(); ++j)
				if (parameterList.parameterDeclarations.get(i).name
						.equals(parameterList.parameterDeclarations.get(j).name))
					throw new SemantException(
							"parameters cannot have the same names");
		
		for (ParameterDecl decl: parameterList.parameterDeclarations)
			env.putEntry(decl.name, new VarEntry(decl.name, decl.type.toType(env)));
	}

	@Override
	public void visit(PrototypeDecl prototypeDecl) {
		exitFunc();
	}

	@Override
	public void visit(RecordDef recordDef) {
	}

	@Override
	public void visit(ReturnStmt returnStmt) {
		if (mayReturn != null) {
			if (!returnStmt.expr.ty.equals(mayReturn))
				throw new SemantException("Two return types conflict");
		} else
			mayReturn = returnStmt.expr.ty;
	}

	@Override
	public void visit(StmtList stmtList) {
	}

	@Override
	public void visit(StringLiteral stringLiteral) {
		stringLiteral.ty = STRING.getInstance();
	}

	@Override
	public void visit(StringType stringType) {
	}

	@Override
	public void visit(SubscriptPostfix subscriptPostfix) {

		if (!subscriptPostfix.expr.ty.equals(STRING.getInstance())
				&& !subscriptPostfix.expr.ty.isArray())
			throw new SemantException(
					"SubscritPostfix Type must be String or Array");

		if (!subscriptPostfix.subscript.ty.equals(INT.getInstance()))
			throw new SemantException("SubscriptPostfix expr must be a int");

		subscriptPostfix.ty = subscriptPostfix.expr.ty.isArray() ? ((ARRAY) subscriptPostfix.expr.ty).elementType
				: CHAR.getInstance();
	}

	@Override
	public void visit(TranslationUnit translationUnit) {
	}

	@Override
	public void visit(UnaryExpr unaryExpr) {
		if (!unaryExpr.expr.ty.equals(INT.getInstance()))
			throw new SemantException("unary Operate on a non-int operator");
		unaryExpr.ty = INT.getInstance();
	}

	@Override
	public void visit(VariableDecl variableDecl) {
		
		if (variableDecl.inRecord)
			return;
		
		Type type = variableDecl.type.toType(env);

		for (Symbol sym : variableDecl.ids.ids) {

			VarEntry varEntry = new VarEntry(sym, type);

			env.putEntry(sym, varEntry);
		}

	}

	@Override
	public void visit(VariableDeclList variableDeclList) {
	}

	@Override
	public void visit(WhileStmt whileStmt) {

		breakOrContinue = null;

		if (!whileStmt.cond.ty.equals(INT.getInstance()))
			throw new SemantException("Not a int valued condition");
	}

}
