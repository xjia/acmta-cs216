package javac.env;

import java.util.*;

import javac.symbol.Symbol;

public class Env {

	private Map<Symbol, Entry> map;

	public Env() {
		map = new HashMap<Symbol, Entry>();
		init();
	}

	public Env(Map<Symbol, Entry> map) {
		this.map = new HashMap<Symbol, Entry>(map);
	}

	public void init() {
		// TODO
	}

	public Entry getEntry(Symbol name) {
		if (!inEnv(name))
			throw new NoNameException(name.toString());
		return map.get(name);
	}
	
	public Entry getEntryWithoutException(Symbol name){
		return map.get(name);
	}

	public void putEntry(Symbol name, Entry entry) {
		if (inEnv(name))
			throw new NameDuplicationException(name.toString());
		map.put(name, entry);
	}

	public boolean inEnv(Symbol name) {
		return map.containsKey(name);
	}

	@Override
	public Env clone() {
		return new Env(this.map);
	}

}
