package javac.env;

import javac.symbol.Symbol;

public class VarEntry extends Entry {

	public javac.type.Type type;

	public static VarEntry transferVarEntry(Entry entry) {
		return (VarEntry) entry;
	}

	public VarEntry(Symbol name, javac.type.Type type) {
		super(name);
		this.type = type;
		init();
	}

	protected void init() {
		// TODO
	}

	public Type getType() {
		return Entry.Type.VAR;
	}
}
