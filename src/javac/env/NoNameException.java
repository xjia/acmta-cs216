package javac.env;

public class NoNameException extends RuntimeException {
	
	private static final long serialVersionUID = -6008363318923516092L;

	public NoNameException(String desp) {
		super(desp + " is not defined.");
	}
}
