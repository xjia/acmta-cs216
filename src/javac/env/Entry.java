package javac.env;

import javac.symbol.Symbol;

public abstract class Entry {

	static public enum Type {
		FUNC, TYPE, VAR
	}

	private Symbol name;

	public Entry(Symbol name) {
		this.name = name;
	}

	public Symbol getName() {
		return name;
	}

	public abstract Type getType();

	@Override
	public String toString() {
		return name.toString();
	}

	public static boolean isFunc(Entry entry) {
		return entry.getType().equals(Type.FUNC);
	}

	public static boolean isVar(Entry entry) {
		return entry.getType().equals(Type.VAR);
	}

	public static boolean isType(Entry entry) {
		return entry.getType().equals(Type.TYPE);
	}

}
