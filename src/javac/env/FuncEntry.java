package javac.env;

import javac.absyn.FunctionHead;

public class FuncEntry extends Entry {

	public FunctionHead head;

	public FuncEntry(FunctionHead head) {
		super(head.functionName);
		this.head = head;
	}

	public Type getType() {
		return Entry.Type.FUNC;
	}

}
