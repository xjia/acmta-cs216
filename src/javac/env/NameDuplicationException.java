package javac.env;

public class NameDuplicationException extends RuntimeException {

	private static final long serialVersionUID = 8166603020239111833L;

	public NameDuplicationException(String name) {
		super(name + " is duplicated in the enviornment.");
	}
}
