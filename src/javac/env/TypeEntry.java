package javac.env;

import javac.symbol.Symbol;

public class TypeEntry extends Entry {

	public javac.type.Type type;

	public static TypeEntry transferTypeEntry(Entry entry) {
		return (TypeEntry) entry;
	}

	public TypeEntry(Symbol name, javac.type.Type type) {
		super(name);
		this.type = type;
		init();
	}

	protected void init() {

	}

	@Override
	public Type getType() {
		return Entry.Type.TYPE;
	}
}
