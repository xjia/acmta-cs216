package javac.absyn;

import javac.util.Position;

public abstract class Stmt extends Node {

	protected Stmt(Position pos) {
		super(pos);
	}
	
	public static final Stmt nop = new ExprStmt(Position.empty, new Null(Position.empty));
}
