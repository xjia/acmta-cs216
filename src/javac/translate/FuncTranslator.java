package javac.translate;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.Stack;

import javac.absyn.*;
import javac.quad.BinOp;
import javac.quad.Branch;
import javac.quad.Call;
import javac.quad.Const;
import javac.quad.Jump;
import javac.quad.Label;
import javac.quad.LabelAddress;
import javac.quad.LabelQuad;
import javac.quad.Mem;
import javac.quad.Move;
import javac.quad.Oprand;
import javac.quad.Quad;
import javac.quad.Return;
import javac.quad.Temp;
import javac.quad.TempOprand;
import javac.semant.SemantException;
import javac.symbol.Symbol;
import javac.type.ARRAY;
import javac.type.CHAR;
import javac.type.INT;
import javac.type.RECORD;
import javac.type.STRING;

public class FuncTranslator {
	
	public static int wordSize = 4;
	HashMap<Symbol, Temp> symbolTable;
	LinkedList<Quad> ir;
	FuncFrag  result;
	Stack<Label> loopcontinue;
	Stack<Label> loopbreak;
	public FuncTranslator(FunctionDef funcdef) {
		loopcontinue = new Stack<Label>();
		loopbreak = new Stack<Label>();	
		ir = new LinkedList<Quad>();
		result = new FuncFrag(new Label(funcdef.head.functionName.toString()));
		symbolTable = new HashMap<Symbol, Temp>();
		Temp[] params = new Temp[funcdef.head.parameterList.parameterDeclarations.size()];
		int i = 0;
		for (ParameterDecl dec: funcdef.head.parameterList.parameterDeclarations) {
			params[i] = new Temp();
			symbolTable.put(dec.name, params[i]);
			i++;
		}
		result.params = params;
		for (VariableDecl dec: funcdef.vardec.variableDeclarations) {
			for (Symbol id: dec.ids.ids)
				symbolTable.put(id, new Temp());
		}
		translate(funcdef.stmts);
		result.body = ir;
	}
	
	public FuncFrag getResult() { 
		return result;
	}
	
	void translate(StmtList stmtlist) {
		for (Stmt stmt: stmtlist.statements) {
			translate(stmt);
		}
	}

	void translate(Stmt stmt) {

		if (stmt instanceof BreakStmt) {
			translate((BreakStmt)stmt);
			return;
		}
		else if (stmt instanceof ContinueStmt) { 
			translate((ContinueStmt)stmt);
			return;
		}
		else if (stmt instanceof CompoundStmt) {
			translate((CompoundStmt)stmt);
			return;
		}
		else if (stmt instanceof ExprStmt) {
			translate((ExprStmt)stmt);
			return;
		}
		else if (stmt instanceof ForStmt) {
			translate((ForStmt)stmt);
			return;
		}
		else if (stmt instanceof IfStmt) {
			translate((IfStmt)stmt);
			return;
		}
		else if (stmt instanceof ReturnStmt) {
			translate((ReturnStmt)stmt);
			return;
		}
		else if (stmt instanceof WhileStmt) {
			translate((WhileStmt)stmt);
			return;
		}
		
		throw new SemantException("Unknow statement " + stmt.toString());
	}

	void translate(WhileStmt stmt) {
		Label begin = new Label();
		Label end = new Label();
		loopcontinue.push(begin);
		loopbreak.push(end);
		ir.add(new LabelQuad(begin));
		ir.add(new Branch(end, translate(stmt.cond), new Const(0), BinaryOp.EQ));
		translate(stmt.body);
		ir.add(new Jump(begin));
		ir.add(new LabelQuad(end));
		loopcontinue.pop();
		loopbreak.pop();
	}

	void translate(ReturnStmt stmt) {
		ir.add(new Return(translate(stmt.expr)));
	}

	void translate(IfStmt stmt) {
		Label mid = new Label();
		Label end = new Label();
		ir.add(new Branch(mid, translate(stmt.cond), new Const(0), BinaryOp.EQ));
		translate(stmt.thenPart);
		ir.add(new Jump(end));
		ir.add(new LabelQuad(mid));
		if(stmt.elsePart != null)
			translate(stmt.elsePart);
		ir.add(new LabelQuad(end));
	}

	void translate(ForStmt stmt) {
		Label begin = new Label();
		Label mid = new Label();
		Label end = new Label();
		loopcontinue.push(mid);
		loopbreak.push(end);
		translate(stmt.init);
		ir.add(new LabelQuad(begin));		
		ir.add(new Branch(end, translate(stmt.cond), new Const(0), BinaryOp.EQ));
		translate(stmt.body);
		ir.add(new LabelQuad(mid));
		translate(stmt.step);
		ir.add(new Jump(begin));
		ir.add(new LabelQuad(end));
		loopcontinue.pop();
		loopbreak.pop();
	}

	void translate(ExprStmt stmt) {
		translate(stmt.expr);
	}

	void translate(CompoundStmt stmt) {
		translate(stmt.stmts);
	}

	void translate(BreakStmt stmt) {
		ir.add(new Jump(loopbreak.peek()));
	}

	void translate(ContinueStmt stmt) {
		ir.add(new Jump(loopcontinue.peek()));
	}

	Oprand translate(Expr expr){
		if (expr instanceof BinaryExpr) 
			return translate((BinaryExpr)expr);
		else if (expr instanceof CharLiteral) 
			return translate((CharLiteral)expr);
		else if (expr instanceof FieldPostfix)
			return translate((FieldPostfix)expr);
		else if (expr instanceof FunctionCall) 
			return translate((FunctionCall)expr);
		else if (expr instanceof Id)
			return translate((Id)expr);
		else if (expr instanceof IntLiteral)
			return translate((IntLiteral)expr);
		else if (expr instanceof NewArray)
			return translate((NewArray)expr);
		else if (expr instanceof NewRecord)
			return translate((NewRecord)expr);
		else if (expr instanceof Null) 
			return translate((Null)expr);
		else if (expr instanceof StringLiteral)
			return translate((StringLiteral)expr);
		else if (expr instanceof SubscriptPostfix)
			return translate((SubscriptPostfix)expr);
		else if (expr instanceof UnaryExpr)
			return translate((UnaryExpr)expr);
		throw new SemantException("Unknown expr");
	}


	TempOprand moveToTemp(Oprand oprand) {
  		if (oprand instanceof TempOprand)
  			return (TempOprand)oprand;
		TempOprand result = new TempOprand(new Temp());
		ir.add(new Move(result, oprand));
		return result;
	}
	
	Oprand translate(UnaryExpr expr) {
		switch(expr.op) {
		case PLUS:
			return translate(expr.expr);
		case MINUS:
			TempOprand dst = new TempOprand(new Temp());
			ir.add(new BinOp(dst, new Const(0), translate(expr.expr), BinaryOp.MINUS));
			return dst;
		case NOT:
			dst = new TempOprand(new Temp());
			Label end = new Label();
			Oprand value = translate(expr.expr);
			ir.add(new Move(dst, new Const(1)));
			ir.add(new Branch(end, value, new Const(0), BinaryOp.EQ));
			ir.add(new Move(dst, new Const(0)));
			ir.add(new LabelQuad(end));
			return dst;
		default:
			throw new SemantException("Unknown Unary Op");
		} 
	}

	Oprand translate(SubscriptPostfix expr) {
		Oprand base = translate(expr.expr);
		Oprand offset = translate(expr.subscript);
		TempOprand address = new TempOprand(new Temp());
		int lengthpos = 0;
        TempOprand t = new TempOprand(new Temp());
		if(!(expr.expr.ty instanceof STRING)) {
			ir.add(new BinOp(t, offset, new Const(wordSize), BinaryOp.MULTIPLY));
			lengthpos = 4;
		}
		ir.add(new BinOp(address, base, t, BinaryOp.PLUS));
		Mem result =  new Mem(address.temp, lengthpos); 
		if (expr.ty instanceof CHAR)
			result.length = 1;
		return result;
	}

	Oprand translate(StringLiteral expr) {
		Label name = new Label();
		TempOprand dst = new TempOprand(new Temp());
		Translator.frags.add(new DataFrag(name, expr.s));
		ir.add(new Move(dst, new LabelAddress(name)));
		return dst;
	}
	Oprand translate(NewRecord expr) {
		TempOprand size = new TempOprand(new Temp());
		ir.add(new Move(size, new Const(((RECORD)expr.ty).fields.size() * 4)));
		return memAlloc(size);
	}

	Oprand translate(Null expr) {
		return new Const(0);
	}

	Oprand translate(NewArray expr) {
		TempOprand dst = new TempOprand(new Temp());
		TempOprand length = new TempOprand(new Temp());
		ir.add(new Move(length, translate(expr.expr)));
		ir.add(new BinOp(dst, length, new Const(1), BinaryOp.PLUS));
		ir.add(new BinOp(dst, dst, new Const(wordSize), BinaryOp.MULTIPLY));
		TempOprand result = memAlloc(dst);
		ir.add(new Move(new Mem(result.temp), length));
		return result;
	}

	TempOprand memAlloc(TempOprand size) {
		Temp[] params = new Temp[1];
		Temp result = new Temp();
		params[0] = size.temp;
		ir.add(new Call(new Label("_malloc"), params, result));
		return new TempOprand(result);
	}

	Oprand translate(IntLiteral expr) {
		return new Const(expr.i);
	}

	Oprand translate(Id expr) {
		return new TempOprand(symbolTable.get(expr.sym));
	}

	Oprand translate(FunctionCall expr) {
		Temp[] params = new Temp[expr.params.size()];
		Temp result = new Temp();
		int i = 0;
		for (Expr arg :expr.params) {
			params[i] = moveToTemp(translate(arg)).temp;
			i++;
		}
		ir.add(new Call(new Label(expr.expr.toString()), params, result));
		return new TempOprand(result);
	}

	Oprand translate(FieldPostfix expr) {
		Temp base = moveToTemp(translate(expr.expr)).temp;
		int offset = 0;
		if (expr.expr.ty instanceof RECORD) {
			offset = ((RECORD)expr.expr.ty).getFieldSeq(expr.field) * wordSize;
			Mem result = new Mem(base, offset);
			if (((RECORD)expr.expr.ty).getFieldType(expr.field) instanceof CHAR)
				result.length = 1;
			return result;
		}
		else if (expr.expr.ty instanceof ARRAY) {
			return new Mem(base, 0);
		}
		else if (expr.expr.ty instanceof STRING) {
			Temp[] params = new Temp[1];
			params[0] = base;
			Call call = new Call(new Label("_strlen"), params, new Temp());
			ir.add(call);
			return new TempOprand(call.result);
		}
		throw new SemantException("unknown field");
	}


	Oprand translate(CharLiteral expr) {
		return new Const((int)expr.c);
	}	

	Oprand translate(BinaryExpr expr) {
		
		switch (expr.op) {
		case MINUS:
		case MULTIPLY:
		case DIVIDE:
		case MODULO:
			TempOprand dst = new TempOprand(new Temp());
			ir.add(new BinOp(dst, translate(expr.l), translate(expr.r), expr.op));
			return dst;

		case COMMA:
			translate(expr.l);
			return translate(expr.r);
		case ASSIGN:
			TempOprand mid = new TempOprand(new Temp());
			Oprand right = translate(expr.r);
			Oprand left = translate(expr.l);
			ir.add(new Move(left, right));
			//ir.add(new Move(translate(expr.l), mid));
			return left;
		case OR:
		case AND:
			dst = moveToTemp(translate(expr.l));
			Label end = new Label();
			LabelQuad endinst = new LabelQuad(end);
			Branch branch = new Branch(end, dst, new Const(0), (expr.op == BinaryOp.AND)?BinaryOp.EQ:BinaryOp.NEQ );
			ir.add(branch);
			ir.add(new Move(dst,translate(expr.r)));
			ir.add(endinst);
			return dst;
		case EQ:
		case NEQ:
		case LESS:
		case GREATER:
		case LESS_EQ:
		case GREATER_EQ:
			end = new Label();
			endinst = new LabelQuad(end);
			branch = new Branch();
			branch.op = expr.op;
			branch.label = end;
			if (expr.l.ty instanceof STRING) {
				Temp[] params = new Temp[2];
				params[0] = moveToTemp(translate(expr.l)).temp;
				params[1] = moveToTemp(translate(expr.r)).temp;
				Call call = new Call(new Label("_strcmp"), params, new Temp());
				ir.add(call);
				branch.left = new TempOprand(call.result);
				branch.right = new Const(0);
			}
			else {
				branch.left = moveToTemp(translate(expr.l));
				branch.right = moveToTemp(translate(expr.r));
			}
			dst = new TempOprand(new Temp());
			ir.add(new Move(dst, new Const(1)));
			ir.add(branch);
			ir.add(new Move(dst, new Const(0)));
			ir.add(endinst);
			return dst;
			
		case PLUS:
			dst = new TempOprand(new Temp());
			if (expr.l.ty instanceof STRING || expr.r.ty instanceof STRING) {
				Temp[] params = new Temp[2];
				if (expr.l.ty instanceof STRING) {
					params[0] = moveToTemp(translate(expr.l)).temp;
				}
				else if (expr.l.ty instanceof INT || expr.l.ty instanceof CHAR) {
					Temp[] intParam = new Temp[1];
					intParam[0] = moveToTemp(translate(expr.l)).temp;
					Temp result = new Temp();
					String toStringCall;
					if (expr.l.ty instanceof INT)
						toStringCall = "_intToString";
					else 
						toStringCall = "_charToString";
					ir.add(new Call(new Label(toStringCall), intParam, result));
					params[0] = result;
				}

				if (expr.r.ty instanceof STRING) {
					params[1] = moveToTemp(translate(expr.r)).temp;
				}
				else if (expr.r.ty instanceof INT || expr.r.ty instanceof CHAR) {
					Temp[] intParam = new Temp[1];
					intParam[0] = moveToTemp(translate(expr.r)).temp;
					Temp result = new Temp();
					String toStringCall;
					if (expr.r.ty instanceof INT)
						toStringCall = "_intToString";
					else 
						toStringCall = "_charToString";
					ir.add(new Call(new Label(toStringCall), intParam, result));
					params[1] = result;
				}
				ir.add(new Call(new Label("_strcat"), params, dst.temp));
			}
			else {
				ir.add(new BinOp(dst, translate(expr.l), translate(expr.r), expr.op));
			}
			return dst;

		default:
			throw new SemantException("No this Operation");
		}		
	}
}
