package javac.translate;

import javac.quad.Label;

public class DataFrag extends Frag {
	public Label label;
	public String data;
	
	public DataFrag(Label l, String d) {
		label = l;
		data = d;
	}
	
	public String toString() {
		return label.toString() + ":\n" + data; 
	}
}
