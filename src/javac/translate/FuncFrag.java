package javac.translate;

import java.util.LinkedList;

import javac.quad.Label;
import javac.quad.Quad;
import javac.quad.Temp;

public class FuncFrag extends Frag{
	public Label function;
	public LinkedList<Quad> body;
	public Temp[] params;
	public FuncFrag(Label name) { 
		function = name;
		body = new LinkedList<Quad>();
	}
	public String toString() {
		String ans = function.toString() + ":\n";
		for (Temp param: params) 
			ans += param.toString() + ",";
		ans += "\n";
		for(Quad q: body) {
			ans += q.toString() + "\n";
		}
		return ans;
	}
}
