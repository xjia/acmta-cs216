package javac.translate;

import java.util.LinkedList;
import javac.absyn.ExternalDecl;
import javac.absyn.FunctionDef;
import javac.absyn.TranslationUnit;

public class Translator {
	static LinkedList<Frag> frags;
	static public LinkedList<Frag> Translate(TranslationUnit absyn) {
		frags = new LinkedList<Frag>();
		for (ExternalDecl dec: absyn.externalDeclarations) {
			if (dec instanceof FunctionDef) {
				FunctionDef funcdef = (FunctionDef)dec;
				FuncTranslator trans = new FuncTranslator(funcdef);
				frags.add(trans.getResult());
			}
		}
		return frags;
	}
	
}
