package javac.translate;

import java.io.FileInputStream;
import java.io.InputStream;

import javac.absyn.TranslationUnit;
import javac.parser.Parser;
import javac.semant.FuncSemant;
import javac.semant.GlobalEnvVisitor;
import javac.semant.RecordSemant;
import javac.semant.Semant;

public class TranslateTest {

	public static void main(String[] args) throws Exception {
		translate("test/bug.java");
	}

	private static void translate(String filename) throws Exception {
		final InputStream in = new FileInputStream(filename);
		final Parser parser = new Parser(in);
		final java_cup.runtime.Symbol parseTree = parser.parse();
		in.close();
		final TranslationUnit translationUnit = (TranslationUnit) parseTree.value;
		GlobalEnvVisitor gev = new GlobalEnvVisitor();
		translationUnit.accept(gev);
        Semant recordSemant = new RecordSemant(gev.getGlobalEnv());
        translationUnit.accept(recordSemant);
        Semant funcSemant = new FuncSemant(recordSemant.getEnv());

        translationUnit.accept(funcSemant);

		for (Frag frag: Translator.Translate(translationUnit)) {
			System.out.println(frag.toString());
		}
	}

}
