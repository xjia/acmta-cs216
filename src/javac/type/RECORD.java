package javac.type;

import java.util.ArrayList;
import java.util.List;

import javac.symbol.Symbol;

public final class RECORD extends Type {

	public static final class RecordField {

		public Type type;
		public Symbol name;
		public int index;

		public RecordField(Type type, Symbol name, int index) {
			this.type = type;
			this.name = name;
			this.index = index;
		}
	}

	public List<RecordField> fields;
	public Symbol name;

	public RECORD(Symbol name) {
		fields = new ArrayList<RecordField>();
		this.name = name;
	}

	@Override
	public boolean equals(Object other) {
		if (other instanceof RECORD) {
			return name.equals(((RECORD) other).name);
		}
		return false;
	}

	@Override
	public boolean isArray() {
		return false;
	}

	@Override
	public boolean isRecord() {
		return true;
	}

	public boolean hasField(Symbol sym) {
		for (RecordField f : fields)
			if (f.name.equals(sym))
				return true;
		return false;
	}

	public Type getFieldType(Symbol b) {
		for (RecordField f : fields)
			if (f.name.equals(b))
				return f.type;

		return null;
		
	}
	public int getFieldSeq(Symbol b) {
		int i = 0;
		for (RecordField f : fields) {
			if (f.name.equals(b))
				return i;
			i++;
		}
		return 0;
	}

}
