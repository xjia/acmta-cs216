JC = javac -d bin -cp bin:lib/commons-lang3-3.1.jar:lib/java-cup-11a-runtime.jar

.PHONY: all parser

all:
	mkdir -p bin
	cd src; find . -name "*.java" > ../sources.txt
	sed -i -e 's#^./#./src/#' sources.txt
	$(JC) @sources.txt

parser:
	cd src/javac/parser; make lexer; make parser

clean:
	rm -rf bin
